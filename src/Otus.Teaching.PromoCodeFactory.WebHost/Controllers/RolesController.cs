﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }
        /// <summary>
        /// Добавить новую роль
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddRoleAsync(RoleInsertModel role)
        {
            try
            {
                await _rolesRepository.AddItemAsync(new Role() { Id = System.Guid.NewGuid() , Name = role.Name, Description = role.Description});
                return new OkResult();
            }
            catch (System.Exception)
            {
                return new StatusCodeResult(500);
            }
        }
        [HttpPut]
        public async Task<ActionResult> UpdateRoleAsync(Role role)
        {
            try
            {
                await _rolesRepository.UpdateItemAsync(role);
                return new OkResult();
            }
            catch (System.Exception)
            {
                return new StatusCodeResult(500);
            }
        }
        [HttpDelete]
        public async Task<ActionResult> DeleteRoleAsync(Role role)
        {
            try
            {
                await _rolesRepository.DeleteItemAsync(role);
                return new OkResult();
            }
            catch (System.Exception)
            {
                return new StatusCodeResult(500);
            }
        }
    }
}