namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleInsertModel
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}