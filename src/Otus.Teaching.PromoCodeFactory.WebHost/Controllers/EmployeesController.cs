﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        [HttpPost]
        public async Task<ActionResult> AddNewEmployee(EmployeeInsertModel employee)
        {
            try
            {
                await _employeeRepository.AddItemAsync(new Employee() {
                                                 Id = System.Guid.NewGuid(), 
                                                 FirstName = employee.FirstName, 
                                                 LastName = employee.LastName, 
                                                 Email = employee.Email, 
                                                 AppliedPromocodesCount = 0,
                                                 Roles = _roleRepository.GetAllAsync().Result.Where(x => employee.Roles.Contains(x.Id)).ToList()
                                                 });
                return new OkResult();
            }
            catch (System.Exception)
            {
                return new StatusCodeResult(500);
            }
        }
        [HttpPut]
        public async Task<ActionResult> UpdateEmployee(Employee employee)
        {
            try
            {
                await _employeeRepository.UpdateItemAsync(employee);
                return new OkResult();
            }
            catch (System.Exception)
            {
                return new StatusCodeResult(500);
            }
        }
        [HttpDelete]
        public async Task<ActionResult> DeleteEmployeeAsync(Employee employee)
        {
            try
            {
                await _employeeRepository.DeleteItemAsync(employee);
                return new OkResult();
            }
            catch (System.Exception)
            {
                return new StatusCodeResult(500);
            }
        }
    }
}